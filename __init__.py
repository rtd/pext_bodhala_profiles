#!/usr/bin/env python3

# Copyright (C) 2016 Sylvia van Os <iamsylvie@openmailbox.org>
#
# Pext OpenWeatherMap module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import os.path
import time
import subprocess
from datetime import datetime
from urllib.request import urlopen
from urllib.error import URLError

from pext_base import ModuleBase
from pext_helpers import Action, SelectionType


class Module(ModuleBase):
    def init(self, settings, q):
        self.key = "c98d3515966557887e4e0c5b656b7001" if ("key" not in settings) else settings['key']
        self.baseUrl = "http://api.openweathermap.org/data/2.5"

        self.q = q

        self.entries = {}
        self.cachedCities = {}
        self.cachedForecasts = {}

        self.scriptLocation = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        
        self._get_entries()
        self._set_main_commands()
        self.curres = None

    def _get_entries(self):
        self.results = { "Super Sample Result": { "name":  "Bodhala Profile Search Results Go Here." } } 
        
        self.q.put([Action.replace_entry_list, sorted( self.results.keys()) ])

    def _set_main_commands(self):
        self.results = { "Super Sample Result": { "name":  "Bodhala Profile Search Results Go Here." } }         
        self.q.put([Action.set_header])
        self.q.put([Action.replace_command_list, [
            "q <profile name>",
            "lf <lawfirm query> [--crawl_id CRAWL_ID] ",
            "c <client query> [--client_id CLIENT_ID]" ]
          ]
        )

    """
    def _get_city_id(self, identifier):
        return self.entries[identifier]['_id']

    def _format_data(self, data):
        return [
                   self._format_place_name(data),
                   self._format_temperature(data),
                   self._format_weather_description(data)
               ]

    def _format_place_name(self, data):
        return "{} ({})".format(data['name'], data["sys"]["country"])

    def _format_temperature(self, data):
        kelvin = data["main"]["temp"]
        celcius = kelvin - 273.15
        fahrenheit = kelvin * 9 / 5 - 459.67
        return "{:.2f} °C / {:.2f} °F".format(celcius, fahrenheit)

    def _format_weather_description(self, data):
        return data["weather"][0]["description"].capitalize()

    def _show_weather(self, cityId):
        # Get and cache the data if not in cache
        if not cityId in self.cachedCities or self.cachedCities[cityId]["time"] < time.time() - 600:
            try:
                httpResponse = urlopen("{}/weather?id={}&appid={}".format(self.baseUrl, cityId, self.key))
            except URLError as e:
                self.q.put([Action.add_error, "Failed to request weather data: {}".format(e)])
                self.q.put([Action.set_selection, []])
                return

            responseData = httpResponse.read().decode("utf-8")
            try:
                data = json.loads(responseData)
            except json.JSONDecodeError as e:
                self.q.put([Action.add_error, "Failed to decode weather data: {}".format(e)])
                self.q.put([Action.set_selection, []])
                return

            if data['cod'] != 200:
                self.q.put([Action.add_error, "Failed to retrieve weather data: {} ({})".format(data['message'], data['cod'])])
                self.q.put([Action.set_selection, []])
                return

            cache = {'time': time.time(), 'data': data}
            self.cachedCities[cityId] = cache

        # Retrieve from cache
        data = self.cachedCities[cityId]["data"]

        # Format and show
        formattedData = [self._format_temperature(data),
                         self._format_weather_description(data)]

        self.q.put([Action.set_header, self._format_place_name(data)])
        self.q.put([Action.replace_command_list, []])
        self.q.put([Action.replace_entry_list, formattedData])

    def _show_forecast(self, cityId, timestamp):
        for forecastEntry in self.cachedForecasts[cityId]["data"]["list"]:
            if forecastEntry["dt"] == timestamp:
                cityData = self.cachedForecasts[cityId]["data"]["city"]
                formattedData = [self._format_temperature(forecastEntry),
                                 self._format_weather_description(forecastEntry)]

                self.q.put([Action.set_header, "{} ({})".format(cityData["name"], cityData["country"])])
                self.q.put([Action.replace_entry_list, formattedData])

    def _retrieve_forecast(self, cityId):
        if not cityId in self.cachedForecasts or self.cachedForecasts[cityId]["time"] < time.time() - 600:
            try:
                httpResponse = urlopen("{}/forecast?id={}&appid={}".format(self.baseUrl, cityId, self.key))
            except URLError as e:
                self.q.put([Action.add_error, "Failed to request weather data: {}".format(e)])
                self.q.put([Action.set_selection, []])
                return

            responseData = httpResponse.read().decode("utf-8") 
            try:
                data = json.loads(responseData)
            except json.JSONDecodeError as e:
                self.q.put([Action.add_error, "Failed to decode weather data: {}".format(e)])
                self.q.put([Action.set_selection, []])
                return

            cache = {'time': time.time(), 'data': data}
            self.cachedForecasts[cityId] = cache

        cityData = self.cachedForecasts[cityId]["data"]["city"]

        self.q.put([Action.set_header, "{} ({})".format(cityData["name"], cityData["country"])])
        self.q.put([Action.replace_command_list, []])
        self.q.put([Action.replace_entry_list, []])

        for forecastEntry in self.cachedForecasts[cityId]["data"]["list"]:
            self.q.put([Action.add_entry, datetime.fromtimestamp(forecastEntry["dt"])])
    """

    def renderResults(self):
        self.renderList( sorted( self.results.keys()) )

    def renderList(self, alist):
        print( "Rendering %s list items" % len(alist) )
        if len(alist) == 0:
            alist = [ "no results" ]
        self.q.put([Action.replace_entry_list,  alist])        

    def stop(self):
        pass

    def selection_made(self, selection):

        stat = os.stat( os.path.realpath( __file__ ) )
        mtime = time.ctime( stat.st_mtime )

        self.q.put(
            [Action.replace_entry_list,
             [
                 "modified: %s" % mtime,
                 "len(selection)=%s" % len(selection)
             ]
           ]
        )
        print( "selection=%s" % selection )
        
        if len(selection) == 0:
            self.results = { "Super Sample Result": { "name":  "Bodhala Profile Search Results Go Here." } }                     
            self.q.put([Action.replace_entry_list, sorted(list(self.results.keys()))])
            self._set_main_commands()
        elif len(selection) == 1:
            qvals = selection[0].get( "value" ).split(" ")
            qcmd = qvals[0].lower()
            qvals = qvals[1:]
            qstr = " ".join( qvals )

            if qcmd == "q":
                self.results = self.bodhalaProfileQuery( qstr )
                
            if qcmd == "lf":
                self.results = self.bodhalaLawfirmQuery( qstr )

            if qcmd == "c":
                # query for clients
                self.results = self.bodhalaClientQuery( qstr )
                
            self.renderResults()
            
            pass

        elif len(selection) == 2:
            keystr = selection[1].get( "value" )
            print( "The user picked a result to view. %s" % keystr )
            self.curres = self.results.get( keystr )
            self.showDoc( self. curres )
            
        elif len(selection) == 3:
            text = selection[2].get( "value" )
            print( text ) # TODO: copy to the clipboard
            self.q.put([Action.copy_to_clipboard, text])
            self.q.put([Action.set_selection, selection[:-1]]) # go back
            # self.showDoc( self.curres )
            

    def showDoc(self, doc):
        jsonstr = json.dumps( doc, indent=4, sort_keys=True)
        self.renderList( jsonstr.split( "\n" ) )

    def bodhalaLawfirmQuery(self, qstr):
        print( "Bodhala query: %s" % qstr )

        # for now we will execute a query via a script.
        if "--" in qstr:
            pass
        else:
            qstr = "--query '%s'" % qstr

        cmd = '/user/rseward/bin/bodhalalawfirm --json %s' % qstr
        print( cmd )
        proc = subprocess.Popen( cmd,  shell=True, stdout=subprocess.PIPE)
        pstdout = proc.communicate()[0]

        print( "type(pstdout)=%s" % type(pstdout) )
        #return { "results":  repr( pstdout) }

        return self.bodhalaParseResults( pstdout.decode( "utf-8" ) )

    def bodhalaClientQuery(self, qstr):
        print( "Bodhala client query: %s" % qstr )

        # for now we will execute a query via a script.
        if "--" in qstr:
            pass
        else:
            qstr = "--query '%s'" % qstr

        cmd = '/user/rseward/bin/bodhalaclient --json %s' % qstr
        print( cmd )
        proc = subprocess.Popen( cmd,  shell=True, stdout=subprocess.PIPE)
        pstdout = proc.communicate()[0]

        print( "type(pstdout)=%s" % type(pstdout) )
        #return { "results":  repr( pstdout) }

        return self.bodhalaParseResults( pstdout.decode( "utf-8" ) )    

    def bodhalaProfileQuery(self, qstr):
        # TODO: could connect to the identity web service.
        print( "Bodhala query: %s" % qstr )

        # for now we will execute a query via a script.
        if "--" in qstr:
            pass
        else:
            qstr = "--full '%s'" % qstr.replace( " ", "_" )

        #proc = subprocess.Popen( '/usr/bin/uptime', shell=True, stdout=subprocess.PIPE)
        cmd = '/user/rseward/bin/bodhalaqueryidentity --json %s' % qstr
        print( cmd )
        proc = subprocess.Popen( cmd,  shell=True, stdout=subprocess.PIPE)
        pstdout = proc.communicate()[0]

        print( "type(pstdout)=%s" % type(pstdout) )
        #return { "results":  repr( pstdout) }

        return self.bodhalaParseResults( pstdout.decode( "utf-8" ) )

        

    def bodhalaParseResults(self, output):
        lines = output.split( "\n" )
        jsonlines = []
        jsonres = False
        print( "lines after split = %s" % len(lines) )
        for l in lines:
            if "Results" in l:
                jsonres = True
                continue
            if jsonres:
                jsonlines.append( l )


        jsonres = {}
        print( "%s" % len(jsonlines) )
        if len(jsonlines):
            jsonstr = "\n".join( jsonlines )
            print( jsonstr )
            jsonres = json.loads( jsonstr  )

        return jsonres
        
        

        

    def process_response(self, response, identifier):
        pass















